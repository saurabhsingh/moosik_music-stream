<?php
    include("includes/classes/Account.php");
    $account = new Account();
    
    include("includes/handlers/register-handler.php");
    include("includes/handlers/login-handler.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Welkomm Moosik</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <div id="inputContainer">
        <form id="loginForm" action="register.php" method="POST">
            <h2>Login to your account</h2>
            <p>
                <label for="loginUsername">Username:</label>
                <input id="loginUsername" name="loginUsername" type="text" placeholder="e.g. JoeDoe" required>
            </p>
            <p>
                <label for="loginPassword">Password:</label>
                <input id="loginPassword" name="loginPassword" type="password" placeholder="********" required>
            </p>
            <button type="submit" name="loginButton">Log in</button>
        </form>

        <form id="registerForm" action="register.php" method="POST">
            <h2>Create your free account</h2>
            <p>
                <?php echo $account->getError("Your username must be between 5 and 25 characters");?>
                <label for="userName">Username:</label>
                <input id="userName" name="userName" type="text" placeholder="e.g. joedoe" required>
            </p>

            <p>
                <?php echo $account->getError("Your username must be between 2 and 25 characters");?>
                <label for="firstName">First name:</label>
                <input id="firstName" name="firstName" type="text" placeholder="e.g. John" required>
            </p>

            <p>
                <?php echo $account->getError("Your last name must be between 5 and 25 characters");?>
                <label for="lastName">Last name:</label>
                <input id="lastName" name="lastName" type="text" placeholder="e.g. Doe" required>
            </p>

            <p>
                <?php echo $account->getError("Your emails don't match");?>
                <label for="email">E-mail:</label>
                <input id="email" name="email" type="email" placeholder="e.g. joedoe@example.com" required>
            </p>
            <p>
                <?php echo $account->getError("Email is invalid");?>
                <label for="email2">Confirm E-mail:</label>
                <input id="email2" name="email2" type="email" placeholder="e.g. JoeDoe" required>
            </p>
            <p>
                <?php echo $account->getError("Your password can only contain numbers and letters");?>
                <?php echo $account->getError("Your password must be greater than 8 characters");?>
                <label for="password">Password:</label>
                <input id="password" name="password" type="password" placeholder="********" required>
            </p>
            <p>
                <?php echo $account->getError("Your password don't match");?>
                <label for="password2">Confirm Password:</label>
                <input id="password2" name="password2" type="password" placeholder="********" required>
            </p>
            <button type="submit" name="registerButton">Sign up</button>
        </form>
    </div>
    
</body>
</html>